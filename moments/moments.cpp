#include <ctime>
#include <iostream>
#include <signal.h>
#include <unistd.h>
#include <cstdlib>
using namespace std;

bool run = true;

int moments[] = {
   0,
/* 012,
 101,
 111,
 123,
 202,
 210,
 222,
 303,
 321,
 333,
 404,
 432,
 444,
 505,
 543,
 555,
 606,
 654,
 707,
 808,
 909, */
1001,
1010,
1111,
1122,
1212,
1221,
1234,
1313,
1331,
1414,
1441,
1515,
1551,
1616,
1717,
1818,
1919,
2002,
2020,
2121,
2222,
2323,
2345,
};

void exitClient(int sig) {
	run = false;
}

int main() {

	signal(SIGINT, exitClient);
	signal(SIGTERM, exitClient);

	int lastMoment = -1;

	system("sudo service ntp restart");
//system("aplay /usr/share/suominen/1876.wav");

	while (run) {
		time_t t = time(0);   // get time now
		struct tm * now = localtime( & t );
		
		int m = (now->tm_hour)*100 + now->tm_min;

		sleep(1);

		for (int i = 0; i < (sizeof(moments)/sizeof(*moments)); i++) {
			if (m != lastMoment && m == moments[i]) {
				system("aplay /usr/share/suominen/1876.wav");
				system("sudo service ntp restart");
				cout << m << endl;
				lastMoment = m;
			}
		}
	}
}
